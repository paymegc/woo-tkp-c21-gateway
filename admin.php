<?php

namespace WooTkpC21Gateway;

use WooTkpC21Gateway\utils\Activator;
use WooTkpC21Gateway\utils\Deactivator;
use WooTkpC21Gateway\utils\Uninstaller;

// If this file is called directly, abort.
defined('ABSPATH') or exit;
defined('WPINC') or die;

// We load Composer's autoload file
require_once plugin_dir_path(__FILE__) . 'vendor/autoload.php';

/**
 * The code that runs during plugin activation.
 */
function activate_woo_tkp_c21_gateway()
{
    Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 */
function deactivate_woo_tkp_c21_gateway()
{
    Deactivator::deactivate();
}

/**
 * The code that runs during plugin uninstallation.
 */
function uninstall_woo_tkp_c21_gateway()
{
    Uninstaller::uninstall();
}

register_activation_hook(__FILE__, '\\' . __NAMESPACE__ . '\activate_woo_tkp_c21_gateway');
register_deactivation_hook(__FILE__, '\\' . __NAMESPACE__ . '\deactivate_woo_tkp_c21_gateway');
register_uninstall_hook(__FILE__, '\\' . __NAMESPACE__ . '\uninstall_woo_tkp_c21_gateway');

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since 1.0.0
 */
function run_woo_tkp_c21_gateway()
{
    $plugin = new Main();
    $plugin->run();
}

run_woo_tkp_c21_gateway();

if (!function_exists('c21Logger')) {
    function c21Logger($msg, $type = "debug")
    {
        if (is_array($msg) || is_object($msg)) {
            $msg = print_r($msg, true);
        }
        $logger = wc_get_logger();

        // source to group your logs.
        $context = array('source' => 'wc-c21-gateway');

        switch ($type) {
            case 'debug':
                $logger->debug($msg, $context);
                break;
            case 'info':
                $logger->info($msg, $context);
                break;
            case 'notice':
                $logger->notice($msg, $context);
                break;
            case 'warning':
                $logger->warning($msg, $context);
                break;
            case 'error':
                $logger->error($msg, $context);
                break;
            case 'critical':
                $logger->critical($msg, $context);
                break;
            case 'alert':
                $logger->alert($msg, $context);
                break;
            case 'emergency':
                $logger->emergency($msg, $context);
                break;
            default:
                $logger->debug($msg, $context);
                break;
        }
    }
}