<?php

namespace WooTkpC21Gateway;

use WC_Admin_Settings;
use WC_Payment_Gateway;
use WooTkpC21Gateway\config\Settings;
use WooTkpC21Gateway\GatewayInterface;
use WooTkpC21Gateway\utils\HelpersTrait;
use WP_Error;

defined('ABSPATH') or exit;
defined('WPINC') or die;

/**
 * Gateway
 *
 * This class provides holds the Gateway functionality
 *
 * @since      1.0.0
 * @author     TeknePay <support@teknepay.com>
 */
class Gateway extends WC_Payment_Gateway
{

    use HelpersTrait;

    /**
     * API credentials
     *
     * @var string
     */
    public $username;
    public $password;

    /**
     * Logging enabled?
     *
     * @var bool
     */
    public $logging;

    /**
     * URL for IPN
     *
     * @var null|string
     */
    public $notifyUrl = null;

    /**
     * @var Reference to logging class.
     */
    //private static $log;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->id = GatewayInterface::PLUGIN_ID;
        $this->method_title = __(GatewayInterface::PLUGIN_TITLE, GatewayInterface::PLUGIN_ID);
        $this->method_description = __(GatewayInterface::PLUGIN_TITLE . ' allows your customers to pay via Check21.', GatewayInterface::PLUGIN_ID);
        $this->has_fields = true;

        // Load the form fields.
        $this->init_form_fields();

        // Load the settings.
        $this->init_settings();

        // Get setting values.
        $this->icon = plugin_dir_url( __FILE__ )."../assets/echeck2.svg";
        $this->title = $this->get_option('title');
        $this->description = $this->get_option('description');
        $this->enabled = $this->get_option('enabled');
        $this->username = $this->get_option('gw_api_username');
        $this->password = $this->get_option('gw_api_password');

        if (is_admin()) {
            add_action('woocommerce_update_options_payment_gateways_' . $this->id, [$this, 'process_admin_options']);
            add_action('admin_notices', [$this, 'admin_notices']);
        }
    }

    /**
     * Initialize Gateway Settings Form Fields
     */
    public function init_form_fields()
    {
        $this->form_fields = Settings::getSettings();
    }

    /**
     * Payment form on checkout page
     */
    public function payment_fields()
    {
        $fields = [
            'js' =>'
                <script>
                    account = document.getElementById("' . $this->id . '-account-no")
                    account.onkeyup = function(e){
                        if(account.value<0){account.value=0}
                        if(account.value>99999999999999999999){account.value=99999999999999999999}
                    }
                    account.onkeypress = function(e){
                        if(account.value<0){account.value=0}
                        if(account.value>99999999999999999999){account.value=99999999999999999999}
                    }
                </script>
            ',
            'style' =>'
                <style>
                /* Chrome, Safari, Edge, Opera */
                input::-webkit-outer-spin-button,
                input::-webkit-inner-spin-button {
                -webkit-appearance: none;
                margin: 0;
                }

                /* Firefox */
                input[type=number] {
                -moz-appearance: textfield;
                }
                </style>
            ',
            'account-type' => '<p class="form-row form-row-wide">
            <label for="' . $this->id . '-account-type">' . __("Account Type", GatewayInterface::PLUGIN_ID) . ' <span class="required">*</span></label>
            <select id="' . $this->id . '-account-type"  name="' . $this->id . '-account-type" 
                style="
                    font-size: 1.5em;
                    padding: 8px;
                    background-repeat: no-repeat;
                    background-position: right .618em center;
                    background-size: 32px 20px;
                    border-color: #c7c1c6;
                    border-top-color: #bbb3b9;
                    width: 100%;
                    background: #fff;
                "
                >
                <option value="C">Checking</option>
                <option value="S">Savings</option>
            </select>
            </p>',
            'routing-no-field' => '<p class="form-row form-row-wide">
			<label for="' . $this->id . '-routing-no">' . __("Routing Number", GatewayInterface::PLUGIN_ID) . ' <span class="required">*</span></label>
			<input onblur="this.value = this.value.replace(/[^0-9]/g, \'\');" id="' . $this->id . '-routing-no" class="input-text" type="text" maxlength="9" autocomplete="off" name="' . $this->id . '-routing-no" />
			</p>',
            'account-no-field' => '<p class="form-row form-row-wide">
			<label for="' . $this->id . '-account-no">' . __("Account Number", GatewayInterface::PLUGIN_ID) . ' <span class="required">*</span></label>
			<input onblur="this.value = this.value.replace(/[^0-9]/g, \'\');" id="' . $this->id . '-account-no" class="input-text " type="number" min="0" max="99999999999999999999" maxlength="20" autocomplete="off" name="' . $this->id . '-account-no" />
			</p>',
        ];

        $html = '<fieldset id="' . $this->id . '-cc-form">'
                . $fields['style']
                . $fields['account-type']
                . $fields['routing-no-field']
                . $fields['account-no-field']
                . $fields['js']
                . '<div class="clear"></div>'
                . '</fieldset>';

        echo $html;
    }

    /**
     * Check if SSL is enabled and notify the user
     */
    public function admin_notices()
    {
        if ('no' === $this->enabled) {
            return;
        }
    }

    public function validate_password_field($key, $value = NULL)
    {
        $post_data = $_POST;

        if ($value == NULL) {
            $value = $post_data['woocommerce_' . GatewayInterface::PLUGIN_ID . '_' . $key];
        }
        if (isset($post_data['woocommerce_' . GatewayInterface::PLUGIN_ID . '_gw_api_username']) && empty($value)) {
            //not validated
            WC_Admin_Settings::add_error(__('Error: You must enter a Gateway API username/password.', GatewayInterface::PLUGIN_ID));

            return false;
        } else {
            API::set_username($post_data['woocommerce_' . GatewayInterface::PLUGIN_ID . '_gw_api_username']);
            API::set_password($value);
        }
        return $value;
    }

    /**
     * Validate checkout form fields
     */
    public function validate_fields()
    {
        $routingNo = isset($_POST[$this->id . '-routing-no']) ? wc_clean($_POST[$this->id . '-routing-no']) : '';
        $accountNo = isset($_POST[$this->id . '-account-no']) ? wc_clean($_POST[$this->id . '-account-no']) : '';

        // Validate $routingNo
        if (empty($routingNo)) {
            if (function_exists('wc_add_notice')) {
                wc_add_notice('Routing Number must be provided.', 'error');
            }
            return new WP_Error(GatewayInterface::PLUGIN_ID . '_error', __('Routing Number must be provided.', GatewayInterface::PLUGIN_ID));
        } elseif (!preg_match('/^\d{9}$/', $routingNo)) {
            if (function_exists('wc_add_notice')) {
                wc_add_notice('Routing Number must be numbers only and 9 digits mandatory.', 'error');
            }
            return new WP_Error(GatewayInterface::PLUGIN_ID . '_error', __('Routing Number must be numbers only and 9 digits mandatory.', GatewayInterface::PLUGIN_ID));
        }

        // Validate $accountNo
        if (empty($accountNo)) {
            if (function_exists('wc_add_notice')) {
                wc_add_notice('Account Number must be provided.', 'error');
            }
            return new WP_Error(GatewayInterface::PLUGIN_ID . '_error', __('Account Number must be provided.', GatewayInterface::PLUGIN_ID));
        } elseif (!preg_match('/\d/', $accountNo)) {
            if (function_exists('wc_add_notice')) {
                wc_add_notice('Account Number must be numbers only.', 'error');
            }
            return new WP_Error(GatewayInterface::PLUGIN_ID . '_error', __('Account Number must be numbers only.', GatewayInterface::PLUGIN_ID));
        }
    }

    /**
     * Process the payment
     *
     * @param int  $order_id Reference.
     * @param bool $retry Should we retry on fail.
     * @param bool $force_customer Force user creation.
     *
     * @throws Exception If payment will not be accepted.
     *
     * @return mixed
     */
    public function process_payment($order_id, $retry = true, $force_customer = false)
    {
        ini_set('display_errors', 'Off'); //notices breaking json

        /**
         * @var $order WC_Order
         */
        $order = wc_get_order($order_id);

        // If order free, do nothing.
        if ($order->get_total() == 0) {
            $order->payment_complete();

            // Return thank you page redirect.
            return [
                'result' => 'success',
                'redirect' => $this->get_return_url($order)
            ];
        }
        $request = API::prepare_data($order, $_POST);
        $response = API::request($request);
        self::log('GW_API_RESPONSE: ' . var_export($response, true));

        switch ($response->{'ResponseCode'}) {
            case 1: // Approved
                add_post_meta($order->id, '_transaction_id', $response->{'Reference'}, true);
                $order->add_order_note(sprintf(__(GatewayInterface::PLUGIN_TITLE . ' charge complete (Transaction ID: %s)', 'woocommerce-gateway-stripe'), $response->{'Reference'}));
                $order->update_status('wc-processing');

                // Remove cart.
                WC()->cart->empty_cart();

                // Return thank you page redirect.
                return [
                    'result' => 'success',
                    'redirect' => $this->get_return_url($order)
                ];
                break;
            
            case 5: // Approved
                add_post_meta($order->id, '_transaction_id', $response->{'Reference'}, true);
                $order->add_order_note(sprintf(__(GatewayInterface::PLUGIN_TITLE . ' charge complete (Transaction ID: %s)', 'woocommerce-gateway-stripe'), $response->{'Reference'}));
                $order->update_status('wc-processing');

                // Remove cart.
                WC()->cart->empty_cart();

                // Return thank you page redirect.
                return [
                    'result' => 'success',
                    'redirect' => $this->get_return_url($order)
                ];
                break;

            default: // Declined / Failed
                break;
        }
    }

}
