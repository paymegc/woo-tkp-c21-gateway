<?php
/**
 * Plugin Name:       Paymegc Tkp C21 Gateway
 * Description:       Check21 Payment Gateway. Our proprietary plugin was developed to facilitate and expedite the integration with our payment gateway. Once installed, our merchants will be able to process payments immediately.
 * Version:           1.21.9.21.2
 */

require 'plugin-update-checker/plugin-update-checker.php';
$myUpdateChecker = Puc_v4_Factory::buildUpdateChecker(
	'https://gitlab.com/paymegc/woo-tkp-c21-gateway',
	__FILE__,
	'woo-tkp-c21-gateway'
);

//Optional: If you're using a private repository, specify the access token like this:
$myUpdateChecker->setAuthentication('MqiAAZzo9WNBAGzgwc3L');

//Optional: Set the branch that contains the stable release.
$myUpdateChecker->setBranch('master');

require_once plugin_dir_path(__FILE__) . 'admin.php';